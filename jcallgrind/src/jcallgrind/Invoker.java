package jcallgrind;

import java.lang.reflect.InvocationTargetException;

class Invoker implements Runnable {

	Class<?> c;
	Arguments a;
	
	public Invoker(Class<?> c, Arguments a) {
		this.c = c;
		this.a = a; //FIXME:Should be a singleton
	}
	
	@Override
	public void run() {
		try {
			c.getMethod("main", String[].class).invoke(null, (Object) a.getInnerArgs());
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			e.printStackTrace();
		}
	}
	
}