package jcallgrind;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Arguments {
	
	private List<String> args;
	private String jarPath = null;
	private String mainClass = null;
	private List<String> innerArgs = null;
	
	private boolean generateCallgraph = false;
	private String callgraphFile = "callgraph.eps";
	private boolean generateSequenceDiagram = false;
	private String sequenceDiagramFile = "sequence-diagram.eps";
	private boolean filterStdLib = false;
	
	public Arguments(String[] args) {
		this.args = Arrays.asList(args);
		innerArgs = new ArrayList<>();
	}
	
	public void parse() {
		if (args.size() < 2) {
			throw new IllegalStateException("No arguments given");
		} else {
			jarPath = args.get(0);
			mainClass = args.get(1);
			
			boolean toInner = false;
			int remainingLength = args.size() - 2;
			if (remainingLength > 0) {
				for (int i = 2; i < args.size(); i++) {
					String arg = args.get(i);
					
					if (!toInner) {
						if (arg.equals("--")) {
							toInner = true;
						} else {
							parseCallgrindArg(arg);
						}
					} else {
						innerArgs.add(arg);
					}
				}
			}
		}
	}
	
	private void parseCallgrindArg(String fullArg) {
		String[] chunks = fullArg.split("=");
		String arg = chunks[0];
		String value = (chunks.length > 1) ? chunks[1] : null;
		
		switch (arg) {
		case "--callgraph":
			generateCallgraph = true;
			
			if (value != null) {
				callgraphFile = value;
			}
			
			break;
		case "--seq-diagram":
			generateSequenceDiagram = true;
			
			if (value != null) {
				sequenceDiagramFile = value;
			}
			break;
		case "--filter-stdlib":
			filterStdLib = true;
			break;
		default:
			throw new IllegalStateException("Invalid argument " + arg);
		}
	}
	
	public String getJarPath() {
		return jarPath;
	}
	
	public String getMainClass() {
		return mainClass;
	}
	
	// Arguments to be passed to 'inner' jar file on main exec
	public String[] getInnerArgs() {
		String[] retArray = new String[innerArgs.size()];
		
		for (int i = 0; i < retArray.length; i++) {
			retArray[i] = innerArgs.get(i);
		}
		
		return retArray;
	}
	
	public boolean generateCallgraph() {
		return generateCallgraph;
	}
	
	public String callgraphFile() {
		return callgraphFile;
	}
	
	public boolean filterStdLib() {
		return filterStdLib;
	}

	public boolean sequenceDiagram() {
		return generateSequenceDiagram;
	}
	
	public String sequenceDiagramFile() {
		return sequenceDiagramFile;
	}
	
}
