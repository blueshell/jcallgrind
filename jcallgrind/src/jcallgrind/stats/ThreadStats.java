package jcallgrind.stats;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ThreadStats {
	
	private Map<String, ClassStats> statsMap;
	private String name;
	
	public ThreadStats(String name) {
		statsMap = new HashMap<String, ClassStats>();
		this.name = name;
	}
	
	public void methodEnter(String className, String methodName, long millis) {
		ClassStats c = ensure(className);
		c.methodEntered(methodName, millis);
	}
	

	public void methodExit(String className, String methodName, long millis) {
		statsMap.get(className).methodExited(methodName, millis);
	}

	private ClassStats ensure(String className) {
		if(!statsMap.containsKey(className))
			statsMap.put(className, new ClassStats(className, name));
		return statsMap.get(className);
	}

	public List<MethodStats> getAllMethodStats() {
		List<MethodStats> methodStats = new ArrayList<>();
		
		for (ClassStats cs : statsMap.values()) {
			methodStats.addAll(cs.getAllMethodStats());
		}
		
		return methodStats;
	}

}
