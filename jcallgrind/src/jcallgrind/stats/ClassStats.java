package jcallgrind.stats;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ClassStats {

	private Map<String, MethodStats> methodStats;
	private String name;
	private String threadName;
	
	public ClassStats(String name, String threadName) {
		methodStats = new HashMap<>();
		this.name = name;
		this.threadName = threadName;
	}
	
	public void methodEntered(String methodName, long millis) {
		MethodStats stats = getMethodStats(methodName);
		stats.pushExecStarted(millis);
	}

	public void methodExited(String methodName, long millis) {
		MethodStats stats = getMethodStats(methodName);
		stats.popExecEnded(millis);
	}
	
	private MethodStats getMethodStats(String methodName) {
		if (!methodStats.containsKey(methodName)) {
			methodStats.put(methodName, new MethodStats("[" + threadName + "] " + name + "." + methodName));
		}
		
		return methodStats.get(methodName);
	}

	public Collection<MethodStats> getAllMethodStats() {
		return methodStats.values();
	}

}
