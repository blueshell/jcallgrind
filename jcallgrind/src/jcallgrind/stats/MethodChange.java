package jcallgrind.stats;


public class MethodChange implements Comparable<MethodChange> {
	
	public enum ChangeType {
		CALL,
		RETURN
	}
	
	private String source; // caller or returning
	private String dest; // callee or returned-to
	private long absTime;

	private ChangeType type;
	
	public MethodChange(String source, String dest, long absTime, ChangeType type) {
		this.source = source;
		this.dest = dest;
		this.absTime = absTime;
		this.type = type;

	}

	public String getSource() {
		return source;
	}
	
	public String getDest() {
		return dest;
	}
	
	public long getTime() {
		return absTime;
	}
	
	public ChangeType getType() {
		return type;
	}
	
	@Override
	public int hashCode() {
		return source.hashCode() + dest.hashCode() + (int) absTime;
	}

	@Override
	public int compareTo(MethodChange other) {
		if (other.absTime == absTime) {
			return 0;
		} else {
			if (other.absTime > absTime) {
				return -1;
			} else {
				return 1;
			}
		}
	}

}
