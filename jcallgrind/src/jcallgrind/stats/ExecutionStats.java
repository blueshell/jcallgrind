package jcallgrind.stats;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set; 

import jcallgrind.stats.MethodChange.ChangeType;

public class ExecutionStats {

	private static ExecutionStats INSTANCE = null;
	private Map<Long, ThreadStats> statsMap;
	private Set<MethodChange> methodCalls;
	private Set<MethodChange> methodReturns;
	
	private ExecutionStats() {
		statsMap = new HashMap<>();
		methodCalls = new HashSet<>();
		methodReturns = new HashSet<>();
	}
	
	public static ExecutionStats getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ExecutionStats();
		}
		
		return INSTANCE;
	}
	
	public synchronized void methodEnter(long threadId, String threadName, String className, String methodName, long millis) {
		ThreadStats t = ensure(threadId, threadName);
		t.methodEnter(className, methodName, millis);
	}
	
	public synchronized void methodExit(long threadId, String className, String methodName, long millis) {
		statsMap.get(threadId).methodExit(className, methodName, millis);
	}
	
	private ThreadStats ensure(long threadId, String threadName) {
		if(!statsMap.containsKey(threadId))
			statsMap.put(threadId, new ThreadStats(threadName));

		return statsMap.get(threadId);
	}

	public synchronized void methodCall(String caller, String callee, long time) {
		methodCalls.add(new MethodChange(caller, callee, time, ChangeType.CALL));
	}
	
	public synchronized void methodReturn(String returning, String returnedTo, long time) {
		methodReturns.add(new MethodChange(returning, returnedTo, time, ChangeType.RETURN));
	}
	
	public Set<MethodChange> getMethodCalls() {
		return methodCalls;
	}
	
	public Set<MethodChange> getMethodReturns() {
		return methodReturns;
	}
	
	public void printStats() {
		for(ThreadStats ts : statsMap.values()){
			ts.getAllMethodStats();
		}
	}
	
	public List<MethodStats> getAllStats() {
		List<MethodStats> stats = new ArrayList<>();
		
		for (ThreadStats ts : statsMap.values()) {
			stats.addAll(ts.getAllMethodStats());
		}
		
		return stats;
	}

}
