package jcallgrind.stats;

import java.text.DecimalFormat;
import java.util.Stack;

public class MethodStats implements Comparable<MethodStats> {

	private long timeConsumed;
	private Stack<Long> execStarted;
	private String name;
	
	public MethodStats(String name) {
		execStarted = new Stack<>();
		this.name = name;
	}
	
	public void pushExecStarted(long millis) {
		execStarted.push(millis);
	}
	
	public void popExecEnded(long millis) {
		assert (execStarted.size() > 0) : "Execution ended before it started";
		
		timeConsumed += (millis - execStarted.pop());
	}
	
	public long getTotalTimeConsumed() {
		return timeConsumed;
	}

	public String getStats(long totalTime) {
		
		float pctage = (float) timeConsumed / (float) totalTime * 100f;
		DecimalFormat fmt = new DecimalFormat("##.##");
		
		return timeConsumed + "\t" + name + "\t(" + fmt.format(pctage) + "%)";
	}

	@Override
	public int compareTo(MethodStats other) {
		if (other.timeConsumed > timeConsumed) {
			return -1;
		} else if (other.timeConsumed < timeConsumed) {
			return 1;
		} else {
			return 0;
		}
	}
	
}
