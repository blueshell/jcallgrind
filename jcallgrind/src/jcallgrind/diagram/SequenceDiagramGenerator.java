package jcallgrind.diagram;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jcallgrind.stats.ExecutionStats;
import jcallgrind.stats.MethodChange;
import jcallgrind.stats.MethodChange.ChangeType;

public class SequenceDiagramGenerator {
	
	private String fileName;
	private List<MethodChange> changes;
	
	public SequenceDiagramGenerator(String fileName) {
		this.fileName = fileName;
		changes = new ArrayList<>();
		
		ExecutionStats execStats = ExecutionStats.getInstance();
		changes.addAll(execStats.getMethodCalls());
		changes.addAll(execStats.getMethodReturns());
		Collections.sort(changes);
	}
	
	private String getPackageName(String qualifiedMethodName) {
		String[] chunks = qualifiedMethodName.split("\\.");		
		StringBuilder sb = new StringBuilder();
		
		int i;
		for (i = 0; i < chunks.length - 3; i++) {
			sb.append(chunks[i]);
			sb.append(".");
		}
		
		sb.append(chunks[i]);
		return sb.toString();
	}

	private String getMethodName(String qualifiedMethodName) {
		String[] chunks = qualifiedMethodName.split("\\.");
		return (chunks.length == 0) ? null : chunks[chunks.length - 1];
	}
	
	private String getClassName(String qualifiedMethodName) {
		String[] chunks = qualifiedMethodName.split("\\.");
		return (chunks.length >= 2) ? chunks[chunks.length - 2] : null;
	}
	
	private String generateSDEditSyntax() {
		StringBuilder prefaceBuf = new StringBuilder();
		//Set<String> classNames = new HashSet<>();
		List<String> classNames = new ArrayList<>();

		StringBuilder connsBuf = new StringBuilder();
		
		for (MethodChange mc : changes) {
			if (mc.getType() == ChangeType.CALL) {
				String sourceName = mc.getSource();
				
				if (!classNames.contains(sourceName))
					classNames.add(sourceName);
				
				String destName = mc.getDest();
				
				if (!classNames.contains(destName))
					classNames.add(destName);
				
				connsBuf.append(getClassName(sourceName));
				connsBuf.append(":");
				connsBuf.append(getClassName(destName));
				connsBuf.append(".");
				connsBuf.append(getMethodName(destName));
				connsBuf.append("()\n");
			}
		}
 		
		Set<String> usedClassNames = new HashSet<>();
		
		for (String fullClassName : classNames) {
			String packageName = getPackageName(fullClassName);
			String className = getClassName(fullClassName);
			
			if (!usedClassNames.contains(className + packageName)) {
				usedClassNames.add(className+packageName);
				
				prefaceBuf.append(className);
				prefaceBuf.append(":");
				prefaceBuf.append(packageName);
				prefaceBuf.append("\n");
			}
		}
		
		return prefaceBuf.toString() + "\n\n" + connsBuf.toString(); 
	}
	
	public void generateDiagram() throws IOException {
		String sdSyntax = generateSDEditSyntax();
		//System.out.println(sdSyntax);
		
		String tmpFileName = "/tmp/" + fileName;
		PrintWriter tmpWriter = new PrintWriter(tmpFileName);
		tmpWriter.print(sdSyntax);
		tmpWriter.close();
		
		Process sdProcess = new ProcessBuilder("java", "-jar", "sdedit-4.01.jar", "-o", fileName, "-t", "pdf", tmpFileName).start();
		InputStream stdout = sdProcess.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(stdout));
		String line;
		while ((line = reader.readLine()) != null) {		
			System.out.println(line);
		}
	}
	
}
