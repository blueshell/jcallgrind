package jcallgrind.diagram;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Set;

import jcallgrind.stats.MethodChange;

public class CallgraphGenerator {

	private Set<MethodCall> methodCalls;
	private String fileName;
	
	public CallgraphGenerator(Set<MethodChange> methodChanges, String fileName) {
		this.fileName = fileName;
		
		methodCalls = new HashSet<>();
		for (MethodChange mc : methodChanges) {
			methodCalls.add((MethodCall) new MethodCall(mc));
		}
	}
	
	// TODO: Filter java.* package stuff?
	private String generateDotSyntax() {
		StringBuilder sb = new StringBuilder();
		sb.append("digraph callgraph {\n");
		sb.append("\tratio=\"fill\";\n");
		sb.append("\tsize=\"8.3,11.7!\";\n");
		
		for (MethodCall mc : methodCalls) {
			sb.append("\t\"");
			sb.append(mc.getSource());
			sb.append("\" -> \"");
			sb.append(mc.getDest());
			sb.append("\"\n");
		}
		
		sb.append("}\n");
		return sb.toString();
	}
	
	public void generateDiagram() throws IOException {
		String dotSrc = generateDotSyntax();
		
		Process p = new ProcessBuilder("dot", "-Teps").start();
		OutputStream stdin = p.getOutputStream();
		InputStream stdout = p.getInputStream();
		
		stdin.write(dotSrc.getBytes());
		stdin.close();
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(stdout));
		PrintWriter writer = new PrintWriter(fileName);
		
		String line;
		while ((line = reader.readLine()) != null) {		
			writer.println(line);
		}
		
		reader.close();
		writer.close();
		
		System.out.println("Callgraph written to " + fileName);
	}
	
	private class MethodCall {
		
		private String source;
		private String dest;
		
		public MethodCall(MethodChange change) {
			this.source = change.getSource();
			this.dest = change.getDest();
		}
		
		public String getSource() {
			return source;
		}
		
		public String getDest() {
			return dest;
		}
		
		@Override
		public int hashCode() {
			return source.hashCode() + dest.hashCode();
		}
		
		@Override
		public boolean equals(Object other) {
			if (other instanceof MethodCall) {
				MethodCall otherMethodCall = (MethodCall) other;
				return source.equals(otherMethodCall.source) &&
						dest.equals(otherMethodCall.dest);
			} else {
				return false;
			}
		}
	}
	
}
