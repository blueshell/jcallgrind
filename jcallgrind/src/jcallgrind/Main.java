package jcallgrind;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import jcallgrind.diagram.CallgraphGenerator;
import jcallgrind.diagram.SequenceDiagramGenerator;
import jcallgrind.runtime.BytecodeInjector;
import jcallgrind.runtime.ExitTrapSecurityManager;
import jcallgrind.runtime.JarClassLoader;
import jcallgrind.stats.ExecutionStats;
import jcallgrind.stats.MethodStats;

public class Main {

	public static void main(String[] args) {
		Arguments arguments = new Arguments(args);

		try {
			arguments.parse();
		} catch (IllegalStateException e) {
			System.out.println("Failed to parse args: " + e.getMessage());
			System.out.println("jcallgrind <path-to-jar> <main.class> [arguments]");
			System.out.println("Arguments:");
			System.out.println("--callgraph[=filename]\tGenerate a callgraph of the system (in EPS format)");
			System.out.println("--seq-diagram[=filename]\tGenerate a sequence diagram of the system (in EPS format)");
			System.out.println("--filter-stdlib\tFilter classes from java.* packages from results");
			return;
		}

		JarClassLoader loader = new JarClassLoader(arguments.getJarPath());
		List<Class<?>> loadedClasses = null;

		try {
			loadedClasses = loader.loadClasses();
		} catch (IOException e) {
			System.out.println("Failed to read jar file");
			return;
		}

		if (loadedClasses == null) {
			System.out.println("Something went badly wrong loading classes");
			return;
		}

		try {
			System.out.println("Injecting loaded classes");
			BytecodeInjector injector = new BytecodeInjector();
			injector.inject(loadedClasses);
		} catch (Exception e) {
			System.out.println("Failed to inject bytecode");
			return;
		}

		Set<Thread> notGeneratedByMain = Thread.getAllStackTraces().keySet();
		ExitTrapSecurityManager etm = new ExitTrapSecurityManager();
		Thread main = null;
		for (Class<?> clazz : loadedClasses) {
			if (clazz.getName().equals(arguments.getMainClass())) {
				System.out.println("Executing main() method");
				System.setSecurityManager(etm);
				main = new Thread(new Invoker(clazz, arguments));
				break;
			}
		}
		if (main == null) {
			System.out.println("Failed to find specified main method");
		} else {
			main.start();
			while(main.isAlive() && !etm.isTrapped())
				try { main.join(1000); } catch (InterruptedException ignored) {ignored.printStackTrace();}
			Set<Thread> generatedByMain = Thread.getAllStackTraces().keySet();
			generatedByMain.removeAll(notGeneratedByMain);
			for (Thread t : generatedByMain) {
				if (!t.equals(Thread.currentThread())) {
					while(t.isAlive() && !etm.isTrapped())
						try { t.join(1000); } catch (InterruptedException e) { e.printStackTrace();
						// FIXME: Don't wait forever	
						}
				}
			}

			System.setSecurityManager(null);
			ExecutionStats execStats = ExecutionStats.getInstance();
			
			List<MethodStats> methodStats = execStats.getAllStats();
			Collections.sort(methodStats);
			Collections.reverse(methodStats);
			
			long totalTime = 0;
			for (MethodStats ms : methodStats) {
				totalTime += ms.getTotalTimeConsumed();
			}
			
			System.out.println("time\t[thread] class.method");
			System.out.println("---------------------------");
			for (MethodStats ms : methodStats) {
				String statsLine = ms.getStats(totalTime);
				
				if (arguments.filterStdLib() && statsLine.contains("java.")) {
					continue;
				}
				
				System.out.println(statsLine);
			}	
			
			if (arguments.generateCallgraph()) {
				CallgraphGenerator cgGen = new CallgraphGenerator(execStats.getMethodCalls(), arguments.callgraphFile());
				
				try {
					cgGen.generateDiagram();
				} catch (IOException e) {
					System.out.println("Diagram generation failed: " + e.getMessage());
				}
			}
			
			if (arguments.sequenceDiagram()) {
				SequenceDiagramGenerator seqGen = new SequenceDiagramGenerator(arguments.sequenceDiagramFile());
				try {
					seqGen.generateDiagram();
				} catch (IOException e) {
					System.out.println("Diagram generation failed: " + e.getMessage());
				}
			}

			if(etm.isTrapped())
				etm.release();
		}
	}
}
