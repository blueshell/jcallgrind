package jcallgrind.runtime;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class JarClassLoader {

	private File jarFilePath;
	
	public JarClassLoader(String jarPath) {
		jarFilePath = new File(jarPath);
	}
	
	public List<Class<?>> loadClasses() throws IOException {
		List<Class<?>> loaded = new ArrayList<>();
		URL jarUrl = jarFilePath.toURI().toURL();
		ClassLoader loader = new URLClassLoader(new URL[]{jarUrl});
		JarFile jarFile = new JarFile(jarFilePath);
		Enumeration<JarEntry> entries = jarFile.entries();
		
		while(entries.hasMoreElements()){
			JarEntry jarEntry = entries.nextElement();
			
			if(jarEntry.isDirectory() || !jarEntry.getName().endsWith(".class"))
				continue;
			
			String className = jarEntry.getName().substring(0, jarEntry.getName().length()-6).replace('/', '.');
			
			try {
				Class<?> c = loader.loadClass(className);
				Class.forName(className, true, loader); // Causes loaded class to be initialized
				loaded.add(c);
			} catch (ClassNotFoundException e) {
				System.out.println("Failed to load class: " + className);
				e.printStackTrace();
			}
		}

		jarFile.close();		
		return loaded;
	}
	
}
