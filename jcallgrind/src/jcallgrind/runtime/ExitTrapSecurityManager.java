package jcallgrind.runtime;

import java.security.Permission;

public class ExitTrapSecurityManager extends SecurityManager {

	
	private boolean trapped;
	
	public ExitTrapSecurityManager() {
		trapped = false;
	}
	
	public boolean isTrapped() {
		return trapped;
	}
	
	public void release() {
		trapped = false;
	}
	
	//Derived from http://stackoverflow.com/questions/5401281/preventing-system-exit-from-api
	@Override
	public void checkPermission(Permission perm) {
		if(perm != null && perm.getName().startsWith("exitVM")) {
			trapped = true;
			while(trapped) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException ignored) {}
			}
		}
	}
	
}
