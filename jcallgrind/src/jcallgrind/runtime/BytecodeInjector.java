package jcallgrind.runtime;

import java.io.IOException;
import java.util.List;

import javassist.CannotCompileException;
import javassist.ClassClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;
import javassist.util.HotSwapper;

import com.sun.jdi.connect.IllegalConnectorArgumentsException;

public class BytecodeInjector {

	private ClassPool classPool = ClassPool.getDefault();
	private HotSwapper hotSwapper;
	
	public BytecodeInjector() throws IOException, IllegalConnectorArgumentsException {
		hotSwapper = new HotSwapper(8000);
	}
	
	public void inject(List<Class<?>> loadedClasses) {
		for (Class<?> clazz : loadedClasses) {
			try {
				injectClass(clazz);
			} catch (IOException | CannotCompileException | NotFoundException e) {
				System.out.println("Failed reloading " + clazz.getName());
			}
		}
	}

	private void injectClass(Class<?> clazz) throws NotFoundException, CannotCompileException, IOException {
		classPool.insertClassPath(new ClassClassPath(clazz));
		CtClass cc = classPool.get(clazz.getName());
		
		for (CtMethod cm : cc.getDeclaredMethods()) {
			try {		
				cm.instrument(new MethodWrappingExprEditor());
			} catch (CannotCompileException e) {
				/* There are a large number of reasons that code might not be compiled
				 * (Class not overriding a method from Object etc.) so this is to be 
				 * expected. Yet to find any reason this needs to be caught and communicated
				 * to the user, so I'm comfortable ignoring it.
				 */
			}
		}
		
		byte[] clazzBytes = cc.toBytecode();
		hotSwapper.reload(clazz.getName(), clazzBytes);
	}
	
	private class MethodWrappingExprEditor extends ExprEditor {
		
		public void edit(MethodCall m) throws CannotCompileException {
			String callingClass = m.where().getDeclaringClass().getName();
			String calledClass = m.getClassName();
			
			String callingMethod = callingClass + "." + m.where().getName();
		    String calledMethod = calledClass + "." + m.getMethodName();
			
			String beforeMethod = 
					"{"
					+ "jcallgrind.stats.ExecutionStats.getInstance().methodCall(\"" + callingMethod + "\", \"" + calledMethod + "\", System.currentTimeMillis());" 
					+ "jcallgrind.stats.ExecutionStats.getInstance().methodEnter(Thread.currentThread().getId(),Thread.currentThread().getName(),\""+m.getClassName()+"\", \""+m.getMethodName()+"\", System.currentTimeMillis());"
					+ "}";  
			String afterMethod = 
					"finally {" 
					+ "jcallgrind.stats.ExecutionStats.getInstance().methodExit(Thread.currentThread().getId(),\""+m.getClassName()+"\", \""+m.getMethodName()+"\", System.currentTimeMillis());"
					+ "jcallgrind.stats.ExecutionStats.getInstance().methodReturn(\"" + calledMethod + "\", \"" + callingMethod + "\", System.currentTimeMillis());"
					+ "}";  
			
			m.replace(beforeMethod + "  try {$_ = $proceed($$); } " + afterMethod);
		}
	}
	
}
