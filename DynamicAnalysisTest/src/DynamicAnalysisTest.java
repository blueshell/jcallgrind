import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.util.HotSwapper;
import notdefault.Testee;

import com.sun.jdi.connect.IllegalConnectorArgumentsException;


public class DynamicAnalysisTest {

	public static void main(String[] args) {
		try {
			DynamicAnalysisTest test = new DynamicAnalysisTest("test/testee.jar");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private Class<?> testee = Testee.class;
	
	public DynamicAnalysisTest() throws NotFoundException, CannotCompileException, IOException, IllegalConnectorArgumentsException {
		Testee testee = new Testee(); // this is gonna be grim to do properly
		munge();
		System.out.println("Class successfully munged");
		
		//Testee testee2 = new Testee();
		//testee2.main(null);
		testee.main(null);
	}
	
	public DynamicAnalysisTest(String jarPath) throws Exception {
		File jarF = new File(jarPath);
		ClassLoader loader = DynamicAnalysisTest.class.getClassLoader();
		JarFile jarFile = new JarFile(jarF);
		Enumeration<JarEntry> entries = jarFile.entries();
		
		while(entries.hasMoreElements()){
			JarEntry jarEntry = entries.nextElement();
			
			// TODO: This should work recursively
			if(jarEntry.isDirectory() || !jarEntry.getName().endsWith(".class"))
				continue;
			
			String className = jarEntry.getName().substring(0, jarEntry.getName().length()-6).replace('/', '.');
			
			if (!className.contains("sun"))
				System.out.println("LOADING: " + className);
			
			Class<?> c = loader.loadClass(className);
			Class.forName(className); // Causes loaded class to be initialized
		}
		
		jarFile.close();
		//loader.close();
		
		munge();
		//testee.newInstance();
		Testee t2 = new Testee();
		t2.main(null);
	}
	
	private void munge() throws NotFoundException, CannotCompileException, IOException, IllegalConnectorArgumentsException {
		ClassPool pool = ClassPool.getDefault();
		CtClass cc = pool.get(testee.getName());
		System.out.println(testee.getName());
		
		for (CtMethod cm : cc.getMethods()) {
			try {
				cm.insertBefore("{ System.out.println(\"Pre: "+ cm.getName() + "()\"); }");
				cm.insertAfter("{ System.out.println(\"Post: "+ cm.getName() + "()\"); }");
				System.out.println("Munged " + cm.getName() + "()");
			} catch (Exception e) {
				System.out.println("Failed to munge " + cm.getName() + "(): " + e.getMessage());
			}
		}
		
		byte[] clazzBytes = cc.toBytecode();
		HotSwapper hs = new HotSwapper(8000);
		hs.reload(testee.getName(), clazzBytes);
	}

}
