package notdefault;

public class Testee {

	public String getName() {
		return "David";
	}
	
	public void sayHello() {
		System.out.println("Hello, " + getName());
	}
	
	public static void main(String[] args) {
		Testee t = new Testee();
		t.sayHello();
	}
	
}
