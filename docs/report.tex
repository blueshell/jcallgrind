\documentclass{article}
\usepackage{graphicx}
\usepackage[hidelinks]{hyperref}

\title{CS409 Assignment 1\\jcallgrind}
\author{David Futcher - 201140412\\Elliot Iddon - 201110611}
\begin{document}
\maketitle
\section{Percentage Contribution}
David Futcher: 50\%\\
Elliot Iddon: 50\%\\
\section{Problem Overview}
The chosen problem is to dynamically analyse the execution path of arbitrary Java programs. Inspired by callgrind\footnote{\url{http://valgrind.org/docs/manual/cl-manual.html}}, jcallgrind analyses the interactions between methods for each thread spawned by the target process. This data can be used to show a breakdown of where time was spent vis-\`{a}-vis callgrind, a callgraph diagram or an interaction diagram.
\section{Outline of Solution and High-level Design Overview}
\subsection{Solution Outline}
Our solution dynamically collects execution data profiling code into classes loaded from a runnable jar at runtime.
There were some concerns that the profiling code could impact on the overall performance of the system but initial tests on moderately large systems such as JHotDraw\footnote{\url{http://jhotdraw.org/}} indicate that this impact is negligible.
\subsection{Design Overview}
Statistics are stored in a Singleton\footnote{\url{https://en.wikipedia.org/wiki/Singleton_pattern}} Object during the child process' main execution. When the execution is complete the resulting analyses is exported in a text-based or graphical format. A limitation of the system is The Halting Problem\footnote{\url{https://en.wikipedia.org/wiki/Halting_problem}}, the target program must either exit with System.exit() or all threads must finish in order for the analyser to output statistics.
\subsection{Libraries and Tools Utilised}
\subsubsection{javassist}
jcallgrind uses javassist\footnote{\url{http://www.csg.ci.i.u-tokyo.ac.jp/~chiba/javassist/}} to modify the bytecode at runtime. This library was selected because it abstracted the production of bytecode to be injected, this is instead achieved by compiling Strings at runtime which made developing this solution significantly easier.
\subsubsection{SDEdit}
SDEdit\footnote{\url{http://sdedit.sourceforge.net/}} is a Java based tool which can generate sequence diagrams from a custom diagram specification language. jcallgrind generates specifications in this language representing the execution of the target program, then pipes this data into SDEdit, which generates a PDF format sequence diagram.
\subsubsection{Graphviz}
Graphviz is a package of several tools used to generate diagrams, developed by AT\&T Labs. Javassist makes use of Graphviz to generate callgraph diagrams, which can be used to visualise the execution of a program. When callgraph generation is enabled, the method call data collected by jcallgrind is transformed into the \emph{dot} language, which is interpreted by Graphviz. 

jcallgrind wraps dot in a Process\footnote{\url{http://docs.oracle.com/javase/7/docs/api/java/lang/Process.html}} and pipes the generated dot language digraphs into Graphviz's \emph{stdin} which generates an eps format callgraph diagram.
\section{Implementation Details}
\subsection{Program Arguments}
jcallgrind's execution is controlled by passing arguments to the program. It also allows command line arguments to be passed to the code being profiled. Arguments available for jcallgrind are \emph{--callgraph}, which generates a callgraph of the program being run, \emph{--seq-diagram}, which generates a sequence diagram of the code being run and \emph{--filter-stdlib}, which provides simple filtering of Java standard library methods from jcallgrind's output.

Differentiating between arguments to be parsed by jcallgrind and arguments to be passed to the target program is achieved by passing a separator consisting of two dashes. Everything after the two dashes argument is passed to the target program.
\subsection{Dealing with System.exit()}
Normally when a call is made to System.exit() the VM shuts down with prejudice. Therefore, a target program calling System.exit() would cause jcallgrind to exit and hence not provide profiling statistics. To avoid this issue a SecurityManager\footnote{\url{http://docs.oracle.com/javase/7/docs/api/java/lang/SecurityManager.html}} is installed prior to executing the target program which traps the call to System.exit() to allow jcallgrind to output its results.
\subsection{Dealing with Threads}
Many (non-trivial) Java programs make use of multiple threads of execution. Javassist supports this by storing a different set of method call information for each thread, keyed by thread id and name. The thread information is then printed out with the rest of the profiling results after the target program's execution has completed. This allows users to more easily identify performance issues in particular threads.
\subsection{Dealing with Loading Classes from Jar}
Java does not provide a simple way to load all of the classes contained within a jar file. This process has to be done manually, by instantiating a JarFile object then iterating over the Enumeration of files contained within. A ClassLoader is used to load the jar's classes into the JVM. Javassist can only work with classes that have been \emph{initialised}, which the standard ClassLoader does not do. Therefore each class must be initialised - somewhat indirectly - by using Class.forName().
\subsection{Profiling Methods With Return In Body}
Javassist provides a way to inject code at the beginning and the end of methods. The initial approach was to inject an execution started method called at the beginning of the method and an execution ended method at the end of the method. However, if the method being instrumented were to return before the full method is executed, jcallgrind's exit method would not be called. To work around this, a \emph{try ... finally} statement was injected, wrapping the original method body. The \emph{finally} block is guaranteed to execute, so jcallgrind's method exit code will always be called.
\section{Results and Evaluation}
To test and evaluate the program several different applications were executed inside jcallgrind.

\begin{itemize}
\item \textbf{Greeter Test} - Simple "Hello, World!" style program with nested function calls
\item \textbf{Thready Test} - Spawns 4 threads that sleep for different amounts of time
\item \textbf{JHotDraw Example} - Example GUI application using the JHotDraw framework
\end{itemize}

\subsection{Execution Time Profiling}
jcallgrind collects timing information for each method executed in the target program. After executing the target program this information is displayed to the user; for each method the following is displayed: the total execution time in the method, percentage of total time taken, the thread the method was executed in, the fully qualified name of the method.

This aspect of the jcallgrind works well on all of the jars tested. The timing information has been tested (using a trivial program calling Thread.sleep()) and is believed to be highly accurate. No significant degradation of performance of target programs was observed, which can be common in profiling applications like jcallgrind. For example, the JHotDraw example's GUI remained responsive when being run under jcallgrind. One limitation of this approach is that nested calls result in double counting for the timing however this information is still regarded as useful because a reduction in time used or number of calls to a sub-chain will be of benefit further up the chain.

Example output (from JHotDraw Example):
\begin{verbatim}
Listening for transport dt_socket at address: 8000
Injecting loaded classes
Executing main() method
time	[thread] class.method
---------------------------
540	[Thread-165] CH.ifa.draw.samples.javadraw.JavaDrawApp.open	(15.98%)
503	[Thread-165] CH.ifa.draw.application.DrawApplication.open	(14.88%)
242	[Thread-165] CH.lstlistingifa.draw.application.DrawApplication.createStatusLine	(7.16%)
100	[AWT-EventQueue-0] CH.ifa.draw.framework.Painter.draw	(2.96%)
97	[AWT-EventQueue-0] CH.ifa.draw.framework.Tool.mouseMove	(2.87%)
97	[AWT-EventQueue-0] CH.ifa.draw.framework.Figure.draw	(2.87%)
95	[Thread-165] CH.ifa.draw.application.DrawApplication.createMenus	(2.81%)
93	[AWT-EventQueue-0] CH.ifa.draw.tool.ConnectionTool.trackConnectors	(2.75%)
93	[Thread-165] CH.ifa.draw.application.DrawApplication.createTools	(2.75%)
[...]
\end{verbatim}

\subsection{Callgraph Generation}
jcallgrind can generate a callgraph using the method call information collected during target program execution. This feature works well with all of the jars tested. However, there are a small number of limitations. This feature is not thread-aware so there is no distinction between threads in the final diagram. For larger programs (like the JHotDraw test program), the callgraph generated is very large and therefore not easily viewable in Evince, however systems designed to view large vectors such as Inkscape are capable of displaying the callgraph.

Example output (from Greeter test):\\
\includegraphics[width=0.6\paperwidth]{greeter-callgraph}

\subsection{Sequence Diagram Generation}
jcallgrind can generate a sequence diagram showing the interactions between classes in the target program. The smaller programs tested (like the Greeter test program) work well with this feature, generating a correct and informative sequence diagram. Larger programs sometimes cause the sequence diagram generation to fail (occurs with the JHotDraw example). This is due to a limitation in the program used to generate the diagrams (SDEdit), where under certain circumstances some inputs fail. As with the callgraph generation, this feature is not thread aware, so some generated diagrams look incorrect (ThreadyTest for example), however diagrams of some use are still generated.

Example output (from Greeter test):\\
\includegraphics[width=0.6\paperwidth]{sequence}
\end{document}
