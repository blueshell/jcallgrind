package testproject;

public class Main {

	public static void main(String[] args) {
		String name = (args.length > 0) ? args[0] : "generically-named-person";
		
		Greeter g = new Greeter(name);
		g.greet();
		System.exit(0);
	}

}
