package testproject;

public class Greeter {

	private String name;
	
	public Greeter(String name) {
		setName(name);
	}
	
	private void setName(String name) {
		this.name = name;
	}
	
	private String getName() {
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return name;
	}
	
	public void greet() {
		String name = getName();
		System.out.println("Hello, " + name + "!");
	}
	
}
