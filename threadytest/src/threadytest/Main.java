package threadytest;

public class Main {
	
	public static void main(String[] args) {
		WaityThread one = new WaityThread(1000);
		WaityThread two = new WaityThread(2000);
		WaityThread three = new WaityThread(3000);
		WaityThread four = new WaityThread(4000);
		one.start();
		two.start();
		three.start();
		four.start();
		/*
		try {
			one.join();
			two.join();
			three.join();
			four.join();
		} catch (InterruptedException e) {
		}
		*/
	}	
}
