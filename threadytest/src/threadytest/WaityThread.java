package threadytest;

public class WaityThread extends Thread{
	
	long millis;
	long finish;
	
	public WaityThread(long millis) {
		this.millis = millis;
	}
	
	@Override
	public void run() {
		finish = System.currentTimeMillis()+millis;
		while(System.currentTimeMillis() < finish) {
			try { Thread.sleep(1000); } catch (InterruptedException ignored) {}
		}
	}
	
}
